# setwd("/Users/alex/Dropbox/Business/Ignyta/Projects/2016-06 Ignyta Task 1/2016-07-13 Web app")

library(shiny)
library(googleVis)
library(BayesLogit)

# runApp()

project_title = "Response Rate Prediction in STARTRK-1 Trial"

source("supportive.r")

shinyServer(function(input, output) {

RunCalculations = reactive({

	# Reference data to define the prior distribution
  prdata = read.csv("shaw_data.csv", header = TRUE, stringsAsFactors=FALSE, na=".")
	X = prdata[,1:3]
	y = prdata[,6:9]
	n = prdata[,"n"]

	# Vector of responses entered by the user
	observed = c(input$cr, input$pr, input$sd, input$pd)

	predicted = ComputePosteriorDistribution(prdata, X,y,n, nnew = observed, alpha=0.05, nsamples=10000, plotit=FALSE)

  # Add complete or partial response
	observed = round(100 * c(observed[1] + observed[2], observed) / sum(observed), 1)

	predicted = round(100 * predicted, 1)

	res = list(observed = observed,
		       predicted = t(predicted)
		       )

    return(res)

})

output$observed_response_table = renderGvis({
 
          res = RunCalculations()  

          response = c("Objective Response (CR + PR)", "   Complete response (CR)", "   Partial response (PR)", "Stable disease (SD)", "Progressive disease or Not evaluable (PD)")

          df = data.frame(response = response, 
                          summary = res$observed)

          colnames(df) = c("Response", "Response rate (%)")

          gvisTable(df)
        
   })

output$predicted_response_table = renderGvis({
 
          res = RunCalculations()  

          response = c("Objective Response (CR + PR)", "   Complete response (CR)", "   Partial response (PR)", "Stable disease (SD)", "Progressive disease or Not evaluable (PD)")

          summary = res$predicted

          summary = rbind(summary[5, ], summary[1, ], summary[2, ], summary[3, ], summary[4, ])

          df = data.frame(response = response, 
                          summary = summary)

          colnames(df) = c("Response", "Response rate (%)", "Lower confidence limit (%)", "Upper confidence limit (%)")

          gvisTable(df)
        
   })

})


