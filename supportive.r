ComputePosteriorDistribution <- function(prdata,X,y,n,nnew=NULL, alpha=0.1, nsamples=100, nburn=100, plotit=TRUE) {

  if(is.null(nnew)) {nnew <-n}
  y <- y/n            # compute average response requried by the program
  J <- ncol(y)        # J is the total number of categories  (here =4 states)
  y<-y[,-ncol(y)]     # removethe last column (wil beused as a reference)
  X<-cbind(rep(1,nrow(prdata)),X)  # add column of 1's for intercept
  S <- nsamples
  P <-ncol(X)
  set.seed(123)
  out = mlogit(y, X, n, samp=S, burn=nburn);
  
  # out$beta stores J-1 matrices of  beta coefficients for modeling outcome values CR, PR, SD
  # in each matrix, rows represent samp  and columns P coefficients (for intercept and 3 dummies for CR, PR, SD)
  
  theta <-matrix(0,S,J) # probability of Y=j at iteration s  (averaged over distribution of x)
  
  # compute probabilities of Y= CR, PR, SD for each configuration of x
  for (s in 1:S) {
    explr <- matrix(0,nrow(X),J-1)
    pY <-  matrix(0,nrow(X),J)
    for (j in 1:(J-1)) {
      betcoef <- out$beta[seq((j-1)*P*S+s,(j-1)*P*S+S*(P-1)+s, by=S)]    
      
      #  a column vector for  exp(xbeta) of Y=j, j=1,..,J-1 for all configurations of x in rows
      explr[,j] <-exp(as.matrix(X)%*%betcoef)
    }
    
    # sums exp logit accross all outcomes for the denominator, vector with entr for each configuration of X
    rsum <- rowSums(explr)
    for (j in 1:(J-1)) {
      pY[,j] <- explr[,j]/(1+rsum) 
    }
    # pY comntins for each configuration of x in rows prob Y=j in columns
    pY[,J] <- 1 - rowSums(pY)
    # compute marginal probabilities given user-specified counts for initial assessment
    pYm <- colSums(pY*nnew)/sum(nnew)
    theta[s,] <-pYm
  }
# add a column for sum of CR and PR
  colnames(theta) <-c("CR", "PR" ,"SD", "PD")
  theta <- as.data.frame(theta)
  theta$CR_PR = theta$CR+theta$PR
  
  if (plotit) {
    par(mfrow=c(2,3))
    for (j in 1:ncol(theta)) {
      plot(density(theta[,j]), main=paste0("Prob(Y=",colnames(theta)[j],")"))
    }    
  }
  theta.point<- colMeans(theta)
  theta.CI <- apply(theta,2,quantile,prob=c(alpha/2, 1-alpha/2))
  res <- rbind(theta.point,theta.CI)
  return(res)
}
