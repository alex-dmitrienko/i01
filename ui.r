

shinyUI(navbarPage(title = "Web app",
  tabPanel("Parameters",

             h3("Response Rate Prediction in STARTRK-1 Trial"),

             fluidRow(
          

                column(4, 


                    wellPanel(

                    h4("Description"),

                    br(),

                    tags$p("This web application performs a Bayesian prediction of response rates based on the Independent Radiology Review (IRR) in the STARTRK-1 trial.")

                    )

                ),

                column(8, 

                    wellPanel(

                    h4("Specify the number of patients in each category based on the Investigator Assessment"),

                    br(),

                    tags$p("Number of patients with complete response:"),

                    numericInput(inputId = "cr", label = "", value = 5, min = 0, max = 100, step = 1),

                    tags$p("Number of patients with partial response:"),

                    numericInput(inputId = "pr", label = "", value = 5, min = 0, max = 100, step = 1),

                    tags$p("Number of patients with stable disease:"),

                    numericInput(inputId = "sd", label = "", value = 5, min = 0, max = 100, step = 1),

                    tags$p("Number of patients with progressive disease or non-evaluable response:"),

                    numericInput(inputId = "pd", label = "", value = 5, min = 0, max = 100, step = 1)

                    )

                )

          )

      ),

  
  tabPanel("Results",

    fluidRow(

        column(12,

             wellPanel(

              h4("Summary of observed response rates"),

              tags$p("Observed response rates based on the Investigator Assessment."),

              br(),

              htmlOutput("observed_response_table")

              )

          )

        ),

     fluidRow(

        column(12,

             wellPanel(

              h4("Summary of predicted response rates"),

              tags$p("Predicted response rates based on the Independent Radiology Review with 95% confidence intervals."),

              br(),

              htmlOutput("predicted_response_table")

              )

          )

        )
   


      )


      )


)